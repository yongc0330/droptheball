﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{

    public float speed = 2f;
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y<-10)
        {
            Debug.Log("Fail");
            OnResetBall();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Booster")
        {
            GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * speed;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="Goal")
        {
            Debug.Log("Goal!!");
            OnResetBall();
        }
    }
    void OnResetBall()
    {
        GamePlayMgr.Instance.StopSimulate();
        transform.position = new Vector3(6.7f, 7.44f, 0f);
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }
}
