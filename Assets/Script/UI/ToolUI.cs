﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToolUI : MonoBehaviour
{
    Button _btn;
    bool _beginDrag = false;
    public GameObject _obstacle;
    // Start is called before the first frame update
    void Awake()
    {
        _btn = GetComponent<Button>();
        //  _btn.onClick.AddListener(BeginTool); 
    }

    // Update is called once per frame
    void Update()
    {

        if (_beginDrag)
        {
            if (Input.GetMouseButtonUp(0))
            {
                BatchObstacle();
            }
        }
        else
        {
            var rect = RectTransformToScreenSpace(GetComponent<RectTransform>());
            if (rect.Contains(Input.mousePosition) && Input.GetMouseButtonDown(0))
            {
                BeginTool();
            }
        }
    }
    public Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        return new Rect((Vector2)transform.position - (size * 0.5f), size);
    }

    private void BeginTool()
    {
        Debug.Log("Start Drag" + gameObject.name);
        _beginDrag = true;
    }

    private void BatchObstacle()
    {
        
        Debug.Log("End Drag" + gameObject.name);

        _beginDrag = false;
        var pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);
        var newPos = Camera.main.ScreenToWorldPoint(pos);
        newPos.z = 0;
        var newObj = GameObject.Instantiate(_obstacle,GameObject.Find("Obstacle").transform);
        newObj.transform.position = newPos;
    }
    
}
