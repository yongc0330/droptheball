﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameUIMgr : MonoSingleton<GameUIMgr>
{
    public Button _startBtn;
    public GameObject _tools;
    // Start is called before the first frame update
    void Start()
    {
        _startBtn.onClick.AddListener(OnClickStartBtn);
    }
    void OnClickStartBtn()
    {
        Debug.Log("push StartBtn");
        _startBtn.gameObject.SetActive(false);
        _tools.SetActive(false);
        GamePlayMgr.Instance.StartSimulate();
    }
    public void ResetStartUI()
    {
        _startBtn.gameObject.SetActive(true);
        _tools.SetActive(true);
    }
}
