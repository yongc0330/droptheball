﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayMgr : MonoSingleton<GamePlayMgr>
{
    GameObject ball;
    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 2f;
        ball = GameObject.Find("Ball").transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartSimulate()
    {
        ball.GetComponent<Rigidbody>().useGravity = true;
    }

    public void StopSimulate()
    {
        GameUIMgr.Instance.ResetStartUI();
        ClearObstacle();
    }
    void ClearObstacle()
    {
        var obsParent = GameObject.Find("Obstacle");

        for(int i=0;i<obsParent.transform.childCount;++i)
        {
            Destroy(obsParent.transform.GetChild(i).gameObject);
        }
    }
}
